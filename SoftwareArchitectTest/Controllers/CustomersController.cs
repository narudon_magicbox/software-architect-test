﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using SoftwareArchitectTest.Models;
using Microsoft.Extensions.Logging;

namespace SoftwareArchitectTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        // GET api/customers
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        //// GET api/customers/5
        //[HttpGet("{id}")]
        //public ActionResult<string> Get(int id)
        //{
        //    return "value";
        //}

        // POST api/customers
        [HttpPost]
        public ActionResult<IEnumerable<string>> Post(Customer customer)
        {

            Console.WriteLine("customer");
            Console.WriteLine(customer);

            //string[] response = { "value1", "value2" };
            //return response;
            //JObject response = JObject.Parse(customer);

            return new string[] { "value1", "value2" };
        }

        //// PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
