﻿using System;
namespace SoftwareArchitectTest.Models
{
    public class Transaction
    {
        public int ID;
        public DateTime createdAt;
        public float amount;
        public string currency;
        public enum Status
        {
            Failed = 0,
            Success = 1,
            Canceled = 2
        };
    }
}
