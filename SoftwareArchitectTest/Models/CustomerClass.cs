﻿using System;

namespace SoftwareArchitectTest.Models
{
    public class Customer
    {
        public int customerID { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public int customer_number  { get; set; }
    }
}
